﻿//int CalculateSquare(int number)
//{

//    return number * number;
//}

//int sqOf5 = CalculateSquare(5);
//int sqOfNegative3 = CalculateSquare(-3);
//int sqOf10 = CalculateSquare(10);

//Console.WriteLine("Square of 5: " + sqOf5);
//Console.WriteLine("Square of -3: " + sqOfNegative3);
//Console.WriteLine("Square of 10: " + sqOf10);

int TinhTongSoChan(int N)
{
    int tong = 0;
    for (int i = 1; i <= N; i++)
    {
        if (i % 2 == 0)
        {
            tong += i;
        }
    }
    return tong;
}
Console.Write("Nhập số nguyên N: ");
int N = Convert.ToInt32(Console.ReadLine());
int tongSoChan = TinhTongSoChan(N);
Console.WriteLine($"Tổng các số chẵn từ 1 đến {N} là: {tongSoChan}");

//int Tongso(int A, int B)
//{
//    int tong = 0;
//    int sobe = Math.Min(A, B);
//    int solon = Math.Max(A, B);
//    for (int i = sobe; i <= solon; i++)
//    {
//        tong += i; 
//    }
//    return tong;
//}
//int tong57 = Tongso(5, 7); 
//int tong75 = Tongso(7, 5); 
//int tong55 = Tongso(5, 5); 

//Console.WriteLine(tong57);
//Console.WriteLine(tong75);
//Console.WriteLine(tong55);

//void A()
//{
//    Console.WriteLine("A Bắt Đầu");
//    B();
//    Console.WriteLine("A Kết Thúc");
//}

//void B()
//{
//    Console.WriteLine("B Bắt Đầu");
//    C();
//    Console.WriteLine("B Kết Thúc");
//}

//void C()
//{
//    Console.WriteLine("C Bắt Đầu & Kết Thúc");
//}

//// Hiển thị chữ tiếng việt
//Console.OutputEncoding = System.Text.Encoding.UTF8;
//A();

//void A(int p)
//{
//    p = 1000;
//}
